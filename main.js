document.getElementById('task-form').addEventListener('submit', function(event) {
    event.preventDefault();
    const taskInput = document.getElementById('task-input');
    const taskList = document.getElementById('task-list');
    if (taskInput.value.trim() !== '') {
        const li = document.createElement('li');
        li.innerHTML = `
            <input type="checkbox">
            <span class="mx-3">${taskInput.value}</span>
            <button class="text-red-500">
                <i class="fas fa-trash-alt"></i>
            </button>
        `;
        taskList.appendChild(li);
        taskInput.value = '';
    }
});
